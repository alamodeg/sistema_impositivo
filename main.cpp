#include <iostream>
#include "Vehiculo.h"
#include "stdio.h"
using namespace std;
#include "Contribuyente.h"
#include "Cuota.h"
#include "Fecha.h"
#include "Infraccion.h"
#include "AgenteTransito.h"
#include "Contravencion.h"
#include "SistemaDeCobro.h"
#include "Recibo.h"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {

	SistemaDeCobro *sistema = new SistemaDeCobro("**SISTEMA DE COBRO**");

	//creo algunas fechas
	Fecha *f1 = new Fecha(28,1,2020);
	Fecha *f2 = new Fecha(28,3,2019);
	Fecha *f3 = new Fecha(28,5,2020);
	Fecha *f4 = new Fecha(28,7,2020);
	Fecha *f5 = new Fecha(28,9,2020);
	Fecha *f6 = new Fecha(28,11,2020);
	
	//creo algunos contribuyentes
	Contribuyente *contribuyente1 = new Contribuyente(true,false,"Miguel","Gomez","Tafi Viejo","000000");
	Contribuyente *contribuyente2 = new Contribuyente(true,false,"Alvaro","Deguer","San Miguel de Tucuman","111111");
	Contribuyente *contribuyente3 = new Contribuyente(false,true,"Maximiliano","Gil Herrera","Concepcion","222222");
	Contribuyente *contribuyente4 = new Contribuyente(false,true,"Silvia","Saracho","Alderetes","333333");
	
	//creo una contravencion
	Contravencion *nuevaContravencion = new Contravencion(001);
	
	//creo 6 cuotas ya que son bimestrales
	Cuota *nuevaCuota1;
	Cuota *nuevaCuota2;
	Cuota *nuevaCuota3;
	Cuota *nuevaCuota4;
	Cuota *nuevaCuota5;
	Cuota *nuevaCuota6;
	
	
	
	Vehiculo *automovil;
	Infraccion *nuevaInfraccion;
	Fecha *f;
	AgenteTransito *agente = new AgenteTransito("Jose","Fernandez","Banda del Rio Sali","1234567"); //creo agente manualmente para no hacer tan largo el menu
	string numMotor,patente,lugar,descrip, formaPago;
	int anio, cant;
	
	
	int opcion;
    
    do {
        system("cls");        // Para limpiar la pantalla
        
        cout << sistema->getNombre() << endl;
        cout << "1. Nuevo Vehiculo" << endl;
        cout << "2. Nueva Infraccion" << endl;
        cout << "3. Ver lista de Vehiculos" << endl;
        cout << "4. Ver Infraccion de un Vehiculo" << endl;
        cout << "5. Ver estado de cuotas de un Vehiculo" << endl;
        cout << "6. Pagar Cuota de un Vehiculo" << endl;
        cout << "7. SALIR" << endl;
        
        cout << "\nIngrese una opcion: ";
        cin >> opcion;
        
        switch (opcion) {
            case 1:            	
            	cout<<"Ingrese numero de motor: ";
            	cin>>numMotor;
            	cout<<"Ingrese patente: ";
            	cin>>patente;
            	cout<<"Ingrese anio de modelo: ";
            	cin>>anio;
            	cout<<"\n";
            	
            	automovil = new Vehiculo(numMotor,patente,anio);
            	
            	automovil->agregarContribuyente(contribuyente1);  //agrego 2 contribuyentes manualmente para no hacer
            	automovil->agregarContribuyente(contribuyente4);  //tan largo el menu
            	
            	//agrego cuotas manualmente para no hacer tan largo el menu
					nuevaCuota1 = new Cuota(2020,1,f1,100,"deuda");
					nuevaCuota2 = new Cuota(2020,2,f2,100,"deuda");
					nuevaCuota3 = new Cuota(2020,3,f3,100,"deuda");
					nuevaCuota4 = new Cuota(2020,4,f4,100,"deuda");
					nuevaCuota5 = new Cuota(2020,5,f5,100,"deuda");
					nuevaCuota6 = new Cuota(2020,6,f6,100,"deuda");
					
            		automovil->agregarCuota(nuevaCuota1);
					automovil->agregarCuota(nuevaCuota2);
					automovil->agregarCuota(nuevaCuota3);
					automovil->agregarCuota(nuevaCuota4);
					automovil->agregarCuota(nuevaCuota5);
					automovil->agregarCuota(nuevaCuota6);
            	
            	sistema->agregarVehiculo(automovil);
            	
            	system("PAUSE");
            	

                break;
                
            case 2: 
            	cout<<"Lugar: ";
            	cin>>lugar;
            	fflush(stdin);
            	cout<<"Descripcion: ";
            	cin>>descrip;
            	fflush(stdin);
            	cout<<"Patente: ";
            	cin>>patente;
            	
            	f = new Fecha(); //agrego fecha actual
            	nuevaInfraccion = new Infraccion(lugar,descrip,agente,f);
            	nuevaInfraccion->agregarContravencion(nuevaContravencion); //agrego contravencion manualmente para no hacer tan largo el menu
            	nuevaInfraccion->CalcularMontoAPagar();
            	sistema->agregarInfraccion(nuevaInfraccion,patente);
            	
				system("PAUSE");
            	

                break;
                
            case 3:
          			sistema->mostrarListaVehiculos();
          			system("PAUSE");
                break;
                
            case 4:
            	cout<<"Ingrese PATENTE: ";
            	cin>>patente;
            	sistema->getInfraccion(patente);
             	system("PAUSE");
                break;
            case 5:
            	cout<<"Ingrese patente del vehiculo: ";
            	cin>>patente;
            	
            	sistema->estadoCuota(patente);
            	
            	system("PAUSE");
            	break;
            case 6:
            	cout<<"Ingrese patente del vehiculo a pagar cuota: ";
            	cin>>patente;
            	cout<<"Ingrese cantidad de cuotas a pagar: ";
            	cin>>cant;
            	cout<<"Forma de pago: ";
            	cin>>formaPago;
            	
            	sistema->PagarCuota(patente,formaPago,cant);
            	
            	string respuesta;
            	cout<<"�Desea imprimir recibos? (si/no) : ";
            	cin>>respuesta;
            	
            	if(respuesta == "si"){
            		sistema->mostrarRecibos();
				}
            	system("PAUSE");
            	break;
        }        
    } while (opcion != 7);   
	
	
	return 0;
}

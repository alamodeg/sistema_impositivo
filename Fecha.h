/*
* Fecha.h
*/

#ifndef FECHA_H_
#define FECHA_H_

#include <iostream>
#include <iomanip>
#include <ctime>
#include <iostream>
using namespace std;

class Fecha {
private:
	short dia;
	short mes;
	short anio;
	short minuto;
	short hora;
	bool esDiaValido() const;
	bool esMesValido() const;
	bool esAnioValido() const;
	short diasEnMes(const short mes) const;
public:
	//Constructores
	Fecha();
	Fecha(const short d, const short m, const short a);
	Fecha(const Fecha &f);
	
	//Setters
	void setFechaActual(void);
	void setFecha(const short d, const short m, const short a);
	
	//Getters
	short getMinuto() const;
	short getHora() const;
	short getDia() const;
	short getMes() const;
	short getAnio() const;
	static short getAnioActual();
	
	bool esAnioBisiesto(void) const;
	long int cantidadDiasDelAnio();
	
	//Sobrecarga de operadores
	friend bool operator>=(Fecha fecha1, Fecha fecha2);
	friend long int operator-(Fecha &fecha1, Fecha &fecha2);//Retorna la diferencia de das entre 2 fechas
	
};

ostream& operator<<(ostream &salida,const Fecha &f);

Fecha::Fecha() {
	setFechaActual();
}
Fecha::Fecha(short int d, short int m, short int a) {
	setFecha(d, m, a);
	hora = 12;
	minuto = 30;
}
Fecha::Fecha(const Fecha &f) :
	dia(f.dia), mes(f.mes), anio(f.anio) {
}

void Fecha::setFechaActual(void) {
	struct tm *ptr;
	time_t sec;
	
	time(&sec);
	ptr = localtime(&sec);
	minuto = (short) ptr->tm_min;
	hora = (short) ptr->tm_hour;
	dia = (short) ptr->tm_mday;
	mes = (short) ptr->tm_mon + 1;
	anio = (short) ptr->tm_year + 1900;
}

void Fecha::setFecha(const short d, const short m, const short a) {
	dia = d;
	mes = m;
	anio = a;
	if(!esAnioValido() || !esMesValido() || !esDiaValido()){
		setFechaActual();
	}
}

bool Fecha::esAnioValido() const {
	return (anio > 0);
}
bool Fecha::esMesValido() const {
	return (mes >= 1 && mes <= 12);
}
bool Fecha::esDiaValido() const {
	return (dia >= 1 && dia <= diasEnMes(mes));
}

short Fecha::diasEnMes(const short m) const{
	short cantidadDias = 0;
	switch (m) {
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
		cantidadDias = 31;
		break;
	case 2:
		if (esAnioBisiesto()) {
			cantidadDias = 29;
		} else {
			cantidadDias = 28;
		}
		break;
	case 4:
	case 6:
	case 9:
	case 11:
		cantidadDias = 30;
		break;
	}
	return cantidadDias;
}


bool Fecha::esAnioBisiesto() const {
	return ((anio % 4 == 0 && anio % 100 != 0) || anio % 400 == 0);
}
short Fecha::getMinuto() const{
	return minuto;
}
short Fecha::getHora() const{
	return hora;
}
short Fecha::getDia() const {
	return dia;
}
short Fecha::getMes() const {
	return mes;
}
short Fecha::getAnio() const {
	return anio;
}
short Fecha::getAnioActual(){
	short year;
	struct tm *ptr;
	time_t sec;
	
	time(&sec);
	ptr = localtime(&sec);
	year = (short) ptr->tm_year + 1900;
	return year;
}

long int Fecha::cantidadDiasDelAnio(){
	long int cantidadDias = 0;
	int febrero=28 + esAnioBisiesto();
	switch (mes) {
	case 1:cantidadDias=dia;break;
	case 2:cantidadDias=31+dia;break;
	case 3:cantidadDias=31+febrero+dia;break;
	case 4:cantidadDias=31+febrero+31+dia;break;
	case 5:cantidadDias=31+febrero+31+30+dia;break;
	case 6:cantidadDias=31+febrero+31+30+31+dia;break;
	case 7:cantidadDias=31+febrero+31+30+31+30+dia;break;
	case 8:cantidadDias=31+febrero+31+30+31+30+31+dia;break;
	case 9:cantidadDias=31+febrero+31+30+31+30+31+31+dia;break;
	case 10:cantidadDias=31+febrero+31+30+31+30+31+31+30+dia;break;
	case 11:cantidadDias=31+febrero+31+30+31+30+31+31+30+31+dia;break;
	case 12:cantidadDias=31+febrero+31+30+31+30+31+31+30+31+30+dia;break;
	}
	return cantidadDias;
}

bool esAnioBisiesto(int anio){
	return ((anio % 4 == 0 && anio % 100 != 0) || anio % 400 == 0);
}
long int cantidadDiasAniosCompletos(int anioBase, int anio){
		
	long int dias=0;
	if(anioBase<anio){
		for(int i=anioBase;i<anio;i++){
			if(esAnioBisiesto(i))
				dias+=366;
			else
				dias+=365;
		}
	}
	return dias;
}
		
bool operator>=(Fecha fecha1, Fecha fecha2){
	return ((fecha1.anio>fecha2.anio) || (fecha1.anio==fecha2.anio && (fecha1.mes>fecha2.mes || (fecha1.mes==fecha2.mes && fecha1.dia>=fecha2.dia))) );
}
		
long int operator-(Fecha &fecha1, Fecha &fecha2){
	long int dif=0;
	if(fecha1>=fecha2){
		long int diasAnios = cantidadDiasAniosCompletos(fecha2.getAnio(), fecha1.getAnio());
		long int dias2=fecha2.cantidadDiasDelAnio();
		long int dias1 =fecha1.cantidadDiasDelAnio() + diasAnios;
		dif=dias1-dias2;
	}
	return dif;
}
		
ostream& operator<<(ostream &salida,const Fecha &f) {
	salida.fill('0');
	salida << setw(2) << f.getDia() << "/" << setw(2) << f.getMes() << "/" << setw(4)
		<< f.getAnio()<<" Hora "<<setw(2)<<f.getHora()<<":"<<setw(2)<<f.getMinuto();
	return salida;
}
		
#endif /* FECHA_H_ */
		

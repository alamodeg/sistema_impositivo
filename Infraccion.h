#ifndef INFRACCION_H_
#define INFRACCION_H_
#include <iostream>
using namespace std;
#include "AgenteTransito.h"
#include "Fecha.h"
#include "Contravencion.h"
#include "Vehiculo.h"

class Infraccion{
	private:
		string lugar;
		string descripcion;
		int monto;
		Fecha *fechaInfraccion;
		AgenteTransito *agente;
		Vehiculo *vehiculo;
		Contravencion *listaContravenciones[10];
		
		int indiceVehiculos;
		int indiceContravenciones;
		
	public:
		Infraccion(string,string,AgenteTransito*, Fecha*,Vehiculo*);
		Infraccion(string ,string ,AgenteTransito *,Fecha *);
		~Infraccion();
		void agregarContravencion(Contravencion*);
		void agregarVehiculo(Vehiculo*);
		void CalcularMontoAPagar();
		void mostrarInfraccion();
		string getDominio();
};

//////////METODOS//////////
Infraccion::Infraccion(string l, string d, AgenteTransito *a,Fecha *f,Vehiculo *vehi){
	this->lugar = l;
	this->descripcion = d;
	this->agente = a;
	this->fechaInfraccion = f;
	this->vehiculo = vehi;
	this->monto = 0;
	
	this->indiceContravenciones = 0;
	this->indiceVehiculos = 0;
}

Infraccion::Infraccion(string l,string d,AgenteTransito *a,Fecha *f){
	this->lugar = l;
	this->descripcion = d;
	this->agente = a;
	this->fechaInfraccion = f;
	this->monto = 0;
	
	this->indiceContravenciones = 0;
	this->indiceVehiculos = 0;
}

void Infraccion::agregarContravencion(Contravencion *c){
	if(this->indiceContravenciones < 10){
		this->listaContravenciones[this->indiceContravenciones] = c;
		this->indiceContravenciones++;
	}
}

void Infraccion::agregarVehiculo(Vehiculo *v){
	this->vehiculo = v;
}

void Infraccion::CalcularMontoAPagar(){
	int aux = 0;
	if(aux < this->indiceContravenciones){
		this->monto = this->monto + this->listaContravenciones[aux]->getUF();
		aux++;
	}
}

void Infraccion::mostrarInfraccion(){
	cout<<"Patente: "<<this->vehiculo->getDominio()<<endl;
	cout<<"Fecha de Infraccion: "<<this->fechaInfraccion->getDia()<<" / "<<this->fechaInfraccion->getMes()<<" / "<<this->fechaInfraccion->getAnio()<<endl;
	cout<<"Lugar de la Infraccion: "<<this->lugar<<endl;
	cout<<"Descripcion: "<<this->descripcion<<endl;
	cout<<"Monto a Pagar:"<<this->monto<<endl;
	
}

string Infraccion::getDominio(){
	return this->vehiculo->getDominio();
}
#endif

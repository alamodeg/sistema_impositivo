#ifndef RECIBO_H_
#define RECIBO_H_
#include <iostream>
using namespace std;
#include "FormaDePago.h"
#include "Efectivo.h"
#include "Tarjeta.h"

class Recibo{
	private:
		static int numRecibo;
		string dominio;
		int periodo;
		Fecha *FechaPago; 
		FormaDePago* FormaPago;
		int CantCuotasaPagar;
		float importe;
	public:
		Recibo();
		Recibo(string,int,Fecha *,int);
		void CrearEfectivo(float,int);
		void CrearTarjeta(string,string,float,int);
		int getCantidadCuotas();
		~Recibo();
		void ImprimirRecibo();
};

int Recibo::numRecibo = 1;

Recibo::Recibo(){
	
}


Recibo::Recibo(string dominio,int periodo,Fecha *fec,int cantcuot){
	this->numRecibo = this->numRecibo + 1;
	this->dominio = dominio;
	this->periodo = periodo;
	this->FechaPago = fec;
	this->CantCuotasaPagar = cantcuot;
}

void Recibo::CrearEfectivo(float monto, int cant){
	Efectivo *formaP = new Efectivo("efectivo");
	this->FormaPago = formaP;
	this->importe = formaP->CalcularMonto(monto,cant);
}

void Recibo::CrearTarjeta(string n, string cod, float monto, int cant){
	Tarjeta *formaP = new Tarjeta(n,cod,"tarjeta");
	this->FormaPago = formaP;
	this->importe = formaP->CalcularMonto(monto,cant);
}

int Recibo::getCantidadCuotas(){
	return this->CantCuotasaPagar;
}

Recibo::~Recibo(){
	
}

void Recibo::ImprimirRecibo(){
	cout <<"\nN� RECIBO: "<<this->numRecibo<<endl;
	cout << "DOMINIO:  " << this->dominio<<endl;
	cout << "PERIODO: " << this->periodo<<endl;
	cout << "FECHA: " << this->FechaPago->getDia()<<"/ "<<this->FechaPago->getMes()<<"/ "<<this->FechaPago->getAnio()<<endl;// posiblemente necesite sobrecarga del operador <<
	cout << "CUOTAS ABONADAS " << this->CantCuotasaPagar<<endl;
	cout << "IMPORTE: $" << this->importe<<endl;
	cout << "FORMA DE PAGO: "<<this->FormaPago->getFormaPago()<<endl;
	cout<<"--------------------"<<endl;
}

#endif

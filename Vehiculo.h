#include <iostream>
using namespace std;
#ifndef VEHICULO_H_
#define VEHICULO_H_
#include "Fecha.h"
#include "Contribuyente.h"
#include "Cuota.h"


class Vehiculo{
	private:
		string numMotor;
		string dominio;
		int anioModelo;
		Contribuyente *listaContribuyentes[10];
		Cuota *listaCuotas[6];

	public:			    
		Vehiculo(string,string,int);
		~Vehiculo();
		bool esAntiguo();
		void agregarContribuyente(Contribuyente *contrib);
		void agregarCuota(Cuota *c);
		void CalcularMontoAPagar(Cuota*);
		string getNumMotor();
		string getDominio();
		int getAnioModelo();	
		Cuota *getCuota(int);
		
		int indiceContrib;
		int indiceCuotas;
};

Vehiculo::Vehiculo(string num, string dom, int anio){
	this->indiceContrib = 0;
	this->indiceCuotas = 0;
	
	this->numMotor = num;
	this->dominio = dom;
	this->anioModelo = anio;
}

//////////METODOS//////////
void Vehiculo::agregarContribuyente(Contribuyente *contrib){
	if(this->indiceContrib < 10){
		this->listaContribuyentes[this->indiceContrib] = contrib;
		this->indiceContrib++;
	}
}

void Vehiculo::agregarCuota(Cuota *c){
	if(this->indiceCuotas < 6){
		this->CalcularMontoAPagar(c);
		this->listaCuotas[this->indiceCuotas] = c;
		this->indiceCuotas++;
	}
}

bool Vehiculo::esAntiguo(){
	Fecha* fActual = new Fecha();
	
	if ((fActual->getAnio() - anioModelo) > 20){
		return true;
	}else{
		return false;
	}
}

void Vehiculo::CalcularMontoAPagar(Cuota *c){
	float montoCalculado = 0;
	Fecha *FechaActual = new Fecha();
	
	if(!this->esAntiguo()){
		if(c->getFechaVencimiento()->getMes() < FechaActual->getMes()){ //si esta vencida por meses
			montoCalculado = c->getMonto() * 1.10;
			c->CalcularMonto(montoCalculado);
		} //si esta vencida por dias 
		if(c->getFechaVencimiento()->getMes() == FechaActual->getMes() && c->getFechaVencimiento()->getDia() < FechaActual->getDia()){
				montoCalculado = c->getMonto() * 1.10;
				c->CalcularMonto(montoCalculado);
		}else{ //si no esta vencida
			montoCalculado = c->getMonto() * 0.85;
			c->CalcularMonto(montoCalculado);
		}
	}else{
		montoCalculado = 0;
		c->CalcularMonto(montoCalculado);
		c->CambiarEstadoCuota("no paga");
	}
}

string Vehiculo::getNumMotor(){
	return this->numMotor;
}

string Vehiculo::getDominio(){
	return this->dominio;
}

int Vehiculo::getAnioModelo(){
	return this->anioModelo;
}

Cuota* Vehiculo::getCuota(int posicion){
	return this->listaCuotas[posicion];
}

#endif

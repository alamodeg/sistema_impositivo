#ifndef AGENTETRANSITO_H_
#define AGENTETRANSITO_H_
#include <iostream>
#include "Persona.h"

class AgenteTransito : public Persona{
	private:
		static int CodigoAgente;
	public:
		AgenteTransito();
		AgenteTransito(string,string,string,string);
		~AgenteTransito();
		int getAgente();
};

int AgenteTransito::CodigoAgente = 1;
//////////METODOS//////////
AgenteTransito::AgenteTransito(string nombre, string apellido, string direccion, string dni) : Persona(nombre,apellido,direccion,dni){
	
}

AgenteTransito::~AgenteTransito(){
	
}

int AgenteTransito::getAgente(){
	return this->CodigoAgente;
}

#endif

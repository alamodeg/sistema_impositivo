#ifndef EFECTIVO_H_
#define EFECTIVO_H_
using namespace std;
#include <iostream>
#include "FormaDePago.h"

class Efectivo : public FormaDePago{
	public:
		Efectivo(string);
		~Efectivo();
		virtual float CalcularMonto(float monto, int cantidad){
			if(cantidad > 2){
				return monto * 0.95 * cantidad;
			}else{
				return monto * cantidad;
			}
		}
};

Efectivo::Efectivo(string tip) : FormaDePago(tip){
	
}

Efectivo::~Efectivo(){
	
}

#endif

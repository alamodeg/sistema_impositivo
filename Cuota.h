#ifndef CUOTA_H_
#define CUOTA_H_
#include <iostream>
using namespace std;
#include "Fecha.h"
#include "Vehiculo.h"

class Cuota{
	private:
		int anio;
		int periodo;
		Fecha *FechaVencimiento;
		float monto;
		string estado;
	public:
		Cuota();
		Cuota(int,int,Fecha*,float,string);
		~Cuota();
		void CalcularMonto(float);
		void CambiarEstadoCuota(string);
		int getAnio();
		int getPeriodo();
		Fecha* getFechaVencimiento();
		float getMonto();
		string getEstado();		
};

Cuota::Cuota(){
	
}

Cuota::Cuota(int anio, int periodo,Fecha *fVenc,float monto,string estado){
	this->anio = anio;
	this->periodo = periodo;
	this->FechaVencimiento = fVenc;
	this->monto = monto;
	this->estado = estado;
}

Cuota::~Cuota() {

}

void Cuota::CambiarEstadoCuota(string estado){
	this->estado = estado;
}

string Cuota::getEstado(){
	return this->estado;
}

Fecha* Cuota::getFechaVencimiento(){
	return this->FechaVencimiento;
}

int Cuota::getAnio(){
	return this->anio;
}

int Cuota::getPeriodo(){
	return this->periodo;
}

float Cuota::getMonto(){
	return this->monto;
}

void Cuota::CalcularMonto(float m){
	this->monto = m;	
}
#endif

#ifndef FORMADEPAGO_H_
#define FORMADEPAGO_H_
using namespace std;
#include <iostream>

class FormaDePago{

	public:
		string tipo;
		FormaDePago(string);
		string getFormaPago();
		virtual float CalcularMonto(float monto, int cantidad) = 0;
};

FormaDePago::FormaDePago(string tip){
	this->tipo = tip;
}

string FormaDePago::getFormaPago(){
	return this->tipo;
}
#endif

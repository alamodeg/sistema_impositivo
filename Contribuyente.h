#ifndef CONTRIBUYENTE_H_
#define CONTRIBUYENTE_H_
#include <iostream>
#include "Persona.h"

class Contribuyente : public Persona{
	private:
		bool esAutorizado;
		bool esPropietario;
	public:
		Contribuyente();
		Contribuyente(bool,bool,string,string,string,string);
		~Contribuyente();
		bool getEsAutorizado();
		bool getEsPropietario();
};

//////////METODOS//////////
Contribuyente::Contribuyente(){
	
}

Contribuyente::Contribuyente(bool aut, bool prop, string nom, string ape,string dir, string dni):Persona(nom,ape,dir,dni){
	this->esAutorizado = aut;
	this->esPropietario = prop;
}

Contribuyente::~Contribuyente(){
	
}

bool Contribuyente::getEsAutorizado(){
	return this->esAutorizado;
}

bool Contribuyente::getEsPropietario(){
	return this->esPropietario;
}

#endif

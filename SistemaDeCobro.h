#include <iostream>
using namespace std;
#define MAX 10
#include <vector>
#include <algorithm>
#include "Vehiculo.h"
#include "Infraccion.h"
#include "Recibo.h"
#include "Cuota.h"
#include "Fecha.h"

class SistemaDeCobro{
	private:
		string nombre;
		Vehiculo *listaVehiculos[MAX];
		Infraccion *listaInfracciones[MAX];
		Recibo *listaRecibos[MAX];
		
		int indiceVehiculos;
		int indiceInfracciones;
	public:
		SistemaDeCobro(string);
		void agregarVehiculo(Vehiculo*);
		void agregarInfraccion(Infraccion*,string);
		void PagarCuota(string,string,int); //metodo para pagar una cuota y setea el atributo de tipo recibo
		void estadoCuota(string);
		Recibo *getRecibo(int);
		string getNombre();
		void getInfraccion(string);
		void mostrarListaVehiculos();
		void mostrarRecibos();
};

SistemaDeCobro::SistemaDeCobro(string n){
	this->nombre = n;
	this->indiceInfracciones = 0;
	this->indiceVehiculos = 0;
}

void SistemaDeCobro::agregarVehiculo(Vehiculo *v){
	if(this->indiceVehiculos < MAX){
		this->listaVehiculos[this->indiceVehiculos] = v;
		this->indiceVehiculos++;
		cout<<"Se agrego el Vehiculo correctamente."<<endl;
	}
}

void SistemaDeCobro::agregarInfraccion(Infraccion *inf,string dom){
	int encontrado = 0;
	for(int i=0; i<this->indiceVehiculos; i++){
		if(this->listaVehiculos[i]->getDominio() == dom){
			if(this->indiceInfracciones < MAX){
				inf->agregarVehiculo(this->listaVehiculos[i]);
				this->listaInfracciones[this->indiceInfracciones] = inf;
				this->indiceInfracciones++;
				encontrado = 1;
			}
		}
	}
	if(encontrado = 0){
		cout<<"NO SE ENCONTRO LA PATENTE EN EL SISTEMA.";
	}

}

void SistemaDeCobro::PagarCuota(string dom, string formaP, int cant){
	int c = 0; //contador para cantidad de cuotas a pagar
	Fecha *fechaActual = new Fecha();
	for(int i=0; i<this->indiceVehiculos; i++){
		if(this->listaVehiculos[i]->getDominio() == dom){
			for(int j=0; j<this->listaVehiculos[i]->indiceCuotas; j++){
				if(c < cant){
					if(listaVehiculos[i]->getCuota(j)->getEstado() == "deuda" && formaP == "efectivo"){
						Recibo *nuevoRecibo = new Recibo(dom,listaVehiculos[i]->getCuota(j)->getPeriodo(), fechaActual, cant); //creo un recibo
						nuevoRecibo->CrearEfectivo(listaVehiculos[i]->getCuota(j)->getMonto(), cant); //creo instancia del tipo efectivo
						this->listaRecibos[c] = nuevoRecibo; //agrego recibo a la lista de recibos
						listaVehiculos[i]->getCuota(j)->CambiarEstadoCuota("pagado"); //cambio estado de la cuota a "pagado"	
						cout<<"Cuota Pagada"<<endl;
						c++;					
					}else{
						if(listaVehiculos[i]->getCuota(j)->getEstado() == "deuda" && formaP == "tarjeta"){
							Recibo *nuevoRecibo = new Recibo(dom,listaVehiculos[i]->getCuota(j)->getPeriodo(), fechaActual, cant); //creo un recibo
							nuevoRecibo->CrearTarjeta("Visa","0123456",listaVehiculos[i]->getCuota(j)->getMonto(), cant); //creo instancia del tipo tarjeta
							this->listaRecibos[c] = nuevoRecibo; //agrego recibo a la lista de recibos
							listaVehiculos[i]->getCuota(j)->CambiarEstadoCuota("pagado"); //cambio estado de la cuota a "pagado"
							cout<<"Cuota Pagada"<<endl;
							c++;
						}
					}
				
				}
			}
		}
	}
	
}

void SistemaDeCobro::estadoCuota(string dom){
	for(int i=0; i<this->indiceVehiculos; i++){
		if(this->listaVehiculos[i]->getDominio() == dom){
			for(int j=0; j<this->listaVehiculos[i]->indiceCuotas; j++){
				if(listaVehiculos[i]->getCuota(j)->getEstado() == "pagado"){
					cout<<"Periodo "<<listaVehiculos[i]->getCuota(j)->getPeriodo()<<": "<<"PAGADO"<<endl;
				}else{
					cout<<"Periodo "<<listaVehiculos[i]->getCuota(j)->getPeriodo()<<": "<<"IMPAGO"<<endl;
				}
			}
		}
	}
}

Recibo *SistemaDeCobro::getRecibo(int posicion){
	return this->listaRecibos[posicion];
}

string SistemaDeCobro::getNombre(){
	return this->nombre;
}

void SistemaDeCobro::getInfraccion(string dom){
	int encontrado = 0;
	for(int i=0; i<this->indiceInfracciones; i++){
		if(this->listaInfracciones[i]->getDominio() == dom){
			encontrado = 1;
			this->listaInfracciones[i]->mostrarInfraccion();
			cout<<"---------------------"<<endl;
		}
	}
	if(encontrado = 0){
		cout<<"NO SE ENCONTRO LA PATENTE EN EL SISTEMA, POSIBLEMENTE EL VEHICULO NO TENGA INFRACCIONES";
	}
}

void SistemaDeCobro::mostrarListaVehiculos(){
	for(int i=0; i<this->indiceVehiculos; i++){
		cout<<"Numero de Motor: "<<this->listaVehiculos[i]->getNumMotor()<<endl;
		cout<<"Patente: "<<this->listaVehiculos[i]->getDominio()<<endl;
		cout<<"Anio de Modelo: "<<this->listaVehiculos[i]->getAnioModelo()<<endl;
		cout<<"\n\n";
	}
}

void SistemaDeCobro::mostrarRecibos(){
	for(int i=0; i<this->getRecibo(0)->getCantidadCuotas();i++){
		this->getRecibo(i)->ImprimirRecibo();
	}
}

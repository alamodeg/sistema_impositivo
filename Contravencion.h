#ifndef CONTRAVENCION_H_
#define CONTRAVENCION_H_
#include <iostream>

class Contravencion{
	private:
		int codigo;
		int uf;
	public:
		Contravencion();
		Contravencion(int);
		int getCodigo();
		int getUF();
};

//////////METODOS//////////
Contravencion::Contravencion(){
	
}

Contravencion::Contravencion(int codigo){
	this->codigo = codigo;
	this->uf = 100;
}

int Contravencion::getCodigo(){
	return this->codigo;
}
int Contravencion::getUF(){
	return this->uf;
}

#endif

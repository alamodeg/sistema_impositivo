#ifndef TARJETA_H_
#define TARJETA_H_
using namespace std;
#include <iostream>
#include "FormaDePago.h"

class Tarjeta : public FormaDePago{
	private:
		string nombre;
		string codigo;
	public:
		Tarjeta();
		Tarjeta(string,string,string);
		string getNombre();
		string getCodigo();
		
		virtual float CalcularMonto(float monto, int cantidad){
			return monto * 1.07 * cantidad;
		}
};

Tarjeta::Tarjeta(string nom, string cod,string tip) : FormaDePago(tip){
	this->nombre = nom;
	this->codigo = cod;
}
string Tarjeta::getNombre(){
	return this->nombre;
}

string Tarjeta::getCodigo(){
	return this->codigo;
}

#endif

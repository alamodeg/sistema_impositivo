#ifndef PERSONA_H_
#define PERSONA_H_
#include <iostream>
using namespace std;



class Persona{
	private:
		string nombre;
		string apellido;
		string direccion;
		string dni;
	public:
		Persona();
		Persona(string,string,string,string);
		string getNombre();
		string getApellido();
		string getDireccion();
		string getDni();
		~Persona();
};

//////////METODOS//////////
Persona::Persona(){
	
}

Persona::Persona(string n,string a, string d, string dni){
	this->nombre = d;
	this->apellido = a;
	this->direccion = d;
	this->dni = dni;
}

string Persona::getNombre(){
	return this->nombre;
}

string Persona::getApellido(){
	return this->apellido;
}
string Persona::getDireccion(){
	return this->direccion;
}
string Persona::getDni(){
	return this->dni;
}

Persona::~Persona(){
	
}

#endif
